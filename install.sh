#!/bin/bash

if ! id | grep -q root; then
	echo "./install.sh must be run as root:"
	echo "sudo ./install.sh"
	exit
fi

dpkg -i ./mesa_22.3.5-1.git20230331.2-0/*.deb

apt-mark hold libd3dadapter9-mesa libd3dadapter9-mesa-dev libegl1-mesa libegl1-mesa-dev libegl-mesa0 libgbm1 libgbm-dev libgl1-mesa-dev libgl1-mesa-dri libgl1-mesa-glx libglapi-mesa libgles2-mesa libgles2-mesa-dev libglx-mesa0 libosmesa6 libosmesa6-dev libwayland-egl1-mesa mesa-common-dev mesa-opencl-icd mesa-va-drivers mesa-vdpau-drivers mesa-vulkan-drivers
